.NOTPARALLEL:

VERSION = 3.30
LOCALE = en

all: tool-check medias

YOUTUBE_URL = https://www.youtube.com/watch?v=bNA-Q8fQqTc
medias: VIDEOS/GNOME-$(VERSION).mkv VIDEOS/GNOME-$(VERSION).srt
	@git lfs fetch
	@git lfs checkout

VIDEOS/GNOME-$(VERSION).mkv:
	@echo "Checking for tools"
	@./check-tools.sh
	@echo "Downloading release video and subtitles"
	@flatpak run --command=youtube-dl --filesystem=home com.github.JannikHv.Gydl -o VIDEOS/GNOME-$(VERSION).'%(ext)s' --all-subs --convert-subs srt $(YOUTUBE_URL)

VIDEOS/GNOME-$(VERSION).srt: VIDEOS/GNOME-$(VERSION).mkv
	@echo "Creating link for $(LOCALE) subtitle"
	@if ! test -f VIDEOS/GNOME-$(VERSION).$(LOCALE).srt ; then echo "*** No subtitle for locale $(LOCALE)" ; exit 1 ; fi
	@ln -s -f -r VIDEOS/GNOME-$(VERSION).$(LOCALE).srt VIDEOS/GNOME-$(VERSION).srt

tool-check: check-tools.sh
	@echo "Checking for tools"
	@./check-tools.sh

user-check: check-user.sh
	@echo "Checking GNOME user, please enter sudo password if necessary"
	@sudo ./check-user.sh
	@set -e ; if test "`id -u gnome 2> /dev/null `" != "`id -u 2> /dev/null`" ; then echo "*** This script should only run as the GNOME user" ; exit 1 ; fi

install-config: polari-initial-setup.py
	@./polari-initial-setup.py

install-data: medias
	for i in DOCUMENTS PICTURES MUSIC VIDEOS; do cp -r $$i/* "`xdg-user-dir $$i`"/ ; done

install: user-check install-config install-data
