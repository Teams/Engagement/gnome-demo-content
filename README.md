# Demo content for GNOME

See below or within each folder for the copyright notices of the media.

To install and deploy the test data, a number of tools are required:
* nm-online (part of NetworkManager tools)
* git-lfs
* Flatpak

## Images, Videos and Documents

From:
https://cloud.gnome.org/index.php/s/a81395e61fd9e241ae97449649d13b8d?path=/Dummy Content

FIXME COPYRIGHT/LICENSE

## GNOME release video

* Video and subtitles, under he CC-BY-SA 3.0 license. See Makefile for source

## Music

* [Neon NiteClub - After Hours](https://www.jamendo.com/en/list/a142959/after-hours) (CC-BY-SA)


## Comics

* [Adventures into the Unknown #1](http://www.archive.org/download/AdventuresIntoTheUnknown1-112/Adventures_Into_The_Unknown_001_1948.cbr) (Public Domain)
* [Edgar Allan Poe - The Raven](http://read.gov/books/pageturner/2003gen37813/#page/2/mode/2up) (Public Domain)

## Books

* All from [Project Gutenberg](http://www.gutenberg.org/) (Public Domain)
