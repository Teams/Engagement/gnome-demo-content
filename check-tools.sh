#!/bin/sh -e

check()
{
	which $1 > /dev/null 2>&1 || (echo "*** $1 not found" ; exit 1)
}

check_flatpak()
{
	flatpak remote-list | grep -q ^flathub || flatpak remote-add --from flathub https://flathub.org/repo/flathub.flatpakrepo
	flatpak info $1 > /dev/null 2>&1 || flatpak install -y flathub $1
}

check nm-online
check git-lfs

echo "Checking for Internet access"
nm-online || (echo "*** Internet connection required" ; exit 1)

check_flatpak org.gnome.Totem
check_flatpak com.github.JannikHv.Gydl
